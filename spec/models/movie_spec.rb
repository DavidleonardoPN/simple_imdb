require 'rails_helper'

RSpec.describe Movie, type: :model do

  let(:admin) { create(:admin) }

  let!(:movie) { create(:movie, title: 'movie_test', admin: admin) }

  describe 'relationships' do
     it { should belong_to(:admin) }
   # it { should has_and_belongs_to_many(:actor) }
  end

  describe 'search scope' do

    it 'value must be equal' do
      expect(Movie.search('movie_test').to_a.first).to eq(movie)
    end

  end

end
