require 'rails_helper'

RSpec.describe Actor, type: :model do
  let(:admin) { create(:admin) }

  let!(:actor) { create(:actor, fullname: 'actor_test', admin: admin) }

  describe 'relationships' do
     it { should belong_to(:admin) }

  end

  describe 'search scope' do

    it 'value must be equal' do
      expect(Actor.search('actor_test').to_a.first).to eq(actor)
    end

  end
end
