FactoryBot.define do
  factory :actor do
    sequence(:fullname)    { |n| "actor-#{n}" }
    sequence(:biografi)    { "test"           }
    sequence(:city)        { |n| "city-#{n}"  }
    association :admin
  end
end
