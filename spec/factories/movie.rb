FactoryBot.define do
  factory :movie do
    sequence(:title)    { |n| "movie-#{n}" }
    sequence(:year)     { |rd| Time.current.year + rd }
    sequence(:rating)   { |n| "#{n}"  }
    association :admin
  end
end
