FactoryBot.define do
  factory :admin do
    sequence(:email) { |n| "person#{n}@example.com" }
    password    { Faker::Internet.password }
    password_confirmation { |u| u.password }
  end
end
