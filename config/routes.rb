Rails.application.routes.draw do
  devise_for :admins, only: [:sessions , :registrations, :passwords]

  devise_scope :admins do
    namespace :admin do
      resources :movies, only: [:new, :create, :edit, :update, :destroy]
      resources :actors, only: [:new, :create, :edit, :update, :destroy]
      resources :producers, only: [:new, :create, :edit, :update, :destroy]
      resources :directors, only: [:new, :create, :edit, :update, :destroy]
    end
  end

  resources :movies, only: [:index, :show] do
    member do
      post :comment
    end
  end

  resources :actors, only: [:index, :show]
  resources :producers, only: [:index, :show]
  resources :directors, only: [:index, :show]

  root 'dashboard#index', as: :dashboard_index

end
