window.Admin ||= {}
window.Admin.Movie ||= {}

Admin.Movie.initForm = ()->
  formatState = (state) ->
    if !state.id
      return state.text
    baseUrl = '/user/pages/images/flags'
    $state = $('<span><img src="' + baseUrl + '/' + state.element.value.toLowerCase() + '.png" class="img-flag" /> ' + state.text + '</span>')

  $('#select2-director').select2
    width: '98%',
    minimumInputLength: 1,
    maximumInputLength: 20 ,
    placeholder: 'Search director'

  $('#select2-producer').select2
    width: '98%',
    minimumInputLength: 1,
    maximumInputLength: 20 ,
    placeholder: 'Search producer'

  $('#select2-actor').select2
    width: '98%',
    minimumInputLength: 1,
    maximumInputLength: 20 ,
    placeholder: 'Search actor'

    $('#select2-genre').select2
      width: '98%',
      placeholder: 'Search genre'



  $('.select2-container').css 'margin-left', '2%'
