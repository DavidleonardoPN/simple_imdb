class Movie < ApplicationRecord
  extend Enumerize

  # relationship
  belongs_to :admin
  has_one_attached :cover
  has_and_belongs_to_many  :actors, join_table: 'movies_actors'
  has_many :comments


  # enumerize
  enumerize :genre, in: [:other, :action, :adventure,
                         :comedy, :crime, :drama, :history,
                        :horor, :musical], default: :other

  # validation
  validates :title, presence: true, uniqueness:true
  validates :year,  presence: true
  validates :cover,  presence: true

  # scope
  scope :search,  ->(search) {
    collection = self

    if search.present?
      collection = collection.where(" (title LIKE :search) OR (year LIKE :search)", search: "%#{search}%")
    end

    collection
  }

end
