class Director < Actor

  # relationship
  has_one_attached :avatar

  # validation
  validates :avatar,  presence: true


  # scope
  scope :search,  ->(search) {
    collection = self

    if search.present?
      collection = collection.where(" (fullname LIKE :search) OR (city LIKE :search)", search: "%#{search}%")
    end

    collection
  }

end
