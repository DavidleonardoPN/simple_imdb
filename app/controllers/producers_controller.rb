class ProducersController < Admin::BaseController
  before_action :find_producer, only: [:show]

  def index
    @producers = Producer.search(params_search).order(id: :desc)
  end

  def show; end

  private

  def find_producer
    @producer = Producer.find(params[:id])
  end

  def params_search
    params[:search]
  end

end
