class DashboardController < ApplicationController

  def index
    @movies     = Movie.limit(4).order(id: :desc)
    @directors  = Director.limit(4).order(id: :desc)
    @actors     = Actor.limit(8).where(type: 'Actor').order(id: :desc)
    @producers  = Producer.limit(4)
  end

end
