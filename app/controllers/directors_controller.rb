class DirectorsController < Admin::BaseController
  before_action :find_director, only: [:show]

  def index
    @directors = Director.search(params_search).order(id: :desc)
  end

  def show; end

  private

  def find_director
    @director = Director.find(params[:id])
  end

  def params_search
    params[:search]
  end

end
