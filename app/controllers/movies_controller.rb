class MoviesController < Admin::BaseController
  before_action :find_movie, only: [:show, :comment]

  def index
    @movies = Movie.search(params_search).order(id: :desc)
  end

  def show; end

  def comment
    @comment = @movie.comments.new(params_comment)

    if @comment.save
      flash[:notice] = "comment has been post"
      redirect_to movie_path(@movie)
    else
      flash[:error] = @comment.errors.full_messages.join("<br />").html_safe
    end

  end

  private

  def find_movie
    @movie = Movie.find(params[:id])
  end

  def params_search
    params[:search]
  end

  def params_comment
    params.require(:comment).permit(:caption)
  end

end
