class ActorsController < Admin::BaseController
  before_action :find_actor, only: [:show]

  def index
    @actors = Actor.search(params_search).where(type: 'Actor').order(id: :desc)
  end

  def show; end

  private

  def find_actor
    @actor = Actor.find(params[:id])
  end

  def params_search
    params[:search]
  end 

end
