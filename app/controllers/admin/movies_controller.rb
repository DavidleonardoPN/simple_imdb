class Admin::MoviesController < Admin::BaseController
  before_action :authenticate_admin!
  before_action :find_movie, only: [:edit, :update, :destroy]


  def index
  end

  def new
    @movie = Movie.new
  end

  def create
    @movie = current_admin.movies.new(movie_params)

    if @movie.save
      flash[:notice] = "#{@movie.title} has been created"
      redirect_to movie_path(@movie)
    else
      flash[:error] = @movie.errors.full_messages.join("<br />").html_safe
    end

  end

  def edit
  end

  def update
    if @movie.update(movie_params)
      flash[:notice] = "#{@movie.title} has been update"
      redirect_to movie_path(@movie)
    else
      flash[:error] = @movie.errors.full_messages.join("<br />").html_safe
    end

  end

  def destroy
    @movie.destroy!
    redirect_to(movies_path)
  end

  def movie_params
    params.require(:movie).permit(:title, :year, :genre, :duration, :sinopsis, :rating,
                                  :cover,actor_ids: [])
  end

  private

  def find_movie
    @movie = current_admin.movies.find(params[:id])
  end

end
