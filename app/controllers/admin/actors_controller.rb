class Admin::ActorsController < Admin::BaseController
  before_action :authenticate_admin!
  before_action :find_actor, only: [:edit, :update, :destroy]
  def new
    @actor = Actor.new
  end

  def create
    @actor = current_admin.actors.new(actor_params.merge({type: Actor}))

    if @actor.save
      flash[:notice] = "#{@actor.fullname} has been created"
      redirect_to actor_path(@actor)
    else
      flash[:error] = @actor.errors.full_messages.join("<br />").html_safe
    end
  end

  def edit; end

  def update
    if @actor.update(actor_params)
      flash[:notice] = "#{@actor.fullname} has been updated"
      redirect_to actor_path(@actor)
    else
      flash[:error] = @actor.errors.full_messages.join("<br />").html_safe
    end
  end

  def destroy
    @actor.destroy!
    redirect_to actors_path
  end

  def actor_params
    params.require(:actor).permit(:fullname, :biografi, :born, :city, :avatar)
  end

  private

  def find_actor
    @actor = current_admin.actors.find(params[:id])
  end

end
