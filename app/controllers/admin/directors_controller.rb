class Admin::DirectorsController < Admin::BaseController
  before_action :authenticate_admin!
  before_action :find_director, only: [:edit, :update, :destroy]

  def new
    @director = Director.new
  end

  def create
    @director = current_admin.directors.new(director_params)

    if @director.save
      flash[:notice] = "#{@director.fullname} has been created"
      redirect_to director_path(@director)
    else
      flash[:error] = @director.errors.full_messages.join("<br />").html_safe
    end
  end

  def edit; end

  def update
    if @director.update(director_params)
      flash[:notice] = "#{@director.fullname} has been updated"
      redirect_to director_path(@director)
    else
      flash[:error] = @director.errors.full_messages.join("<br />").html_safe
    end
  end

  def destroy
    @director.destroy!
  end


  def director_params
    params.require(:director).permit(:fullname, :biografi, :born, :city, :avatar)
  end

  private

  def find_director
    @director = current_admin.directors.find(params[:id])
  end


end
