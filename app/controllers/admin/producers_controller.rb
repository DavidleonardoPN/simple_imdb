class Admin::ProducersController < Admin::BaseController
  before_action :authenticate_admin!
  before_action :find_producer, only: [:edit, :update, :destroy]

  def new
    @producer = Producer.new
  end

  def create
    @producer = current_admin.producers.new(producer_params)

    if @producer.save
      flash[:notice] = "#{@producer.fullname} has been created"
      redirect_to producer_path(@producer)
    else
      flash[:error] = @producer.errors.full_messages.join("<br />").html_safe
    end

  end

  def edit; end

  def update
    if @producer.update(producer_params)
      flash[:notice] = "#{@producer.fullname} has been updated"
      redirect_to producer_path(@producer)
    else
      flash[:error] = @producer.errors.full_messages.join("<br />").html_safe
    end

  end

  def destroy
    @producer.destroy!
  end

  def producer_params
    params.require(:producer).permit(:fullname, :biografi, :born, :city,
                                  :avatar)
  end

  private

  def find_producer
    @producer = current_admin.producers.find(params[:id])
  end

end
