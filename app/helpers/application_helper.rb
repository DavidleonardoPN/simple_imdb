module ApplicationHelper

  def call_js_init(some_js)
    content_for :javascript do
      "<script type='text/javascript'>
          $(document).ready(function(){
            #{some_js}
          });
      </script>".html_safe
    end
  end

  def duration_helper(duration)
    duration.strftime("%H Hours %M Minutes")
  end

  def born_helper(born)
    born.strftime("%d %B %Y")
  end

  def post_helper(post)
    post.strftime("%d %B %Y")
  end

end
