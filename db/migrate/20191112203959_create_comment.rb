class CreateComment < ActiveRecord::Migration[6.0]
  def change
    create_table :comments do |t|
      t.text :caption
      t.references :movie, index: true
      t.timestamps
    end
  end
end
