class AddReferenceActorToAdmin < ActiveRecord::Migration[6.0]
  def change
    add_reference :actors, :admin, index: true
  end
end
