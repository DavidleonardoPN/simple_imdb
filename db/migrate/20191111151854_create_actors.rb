class CreateActors < ActiveRecord::Migration[6.0]
  def change
    create_table :actors do |t|
      t.string :fullname
      t.text :biografi
      t.date   :born
      t.string :city

      t.timestamps
    end
  end
end
