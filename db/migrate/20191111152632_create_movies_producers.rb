class CreateMoviesProducers < ActiveRecord::Migration[6.0]
  def change
    create_table :movies_producers do |t|
      t.references :movie, index: true
      t.references :actor, index: true, foreign_key: 'producer_id'
      t.timestamps  null: false
    end
  end
end
