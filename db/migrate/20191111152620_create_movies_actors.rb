class CreateMoviesActors < ActiveRecord::Migration[6.0]
  def change
    create_table :movies_actors do |t|
      t.references :movie, index: true
      t.references :actor, index: true, foreign_key: 'actor_id'
      t.timestamps null: false
    end
  end
end
