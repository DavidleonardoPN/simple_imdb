class AddReferenceMovieToAdmin < ActiveRecord::Migration[6.0]
  def change
     add_reference :movies, :admin, index: true
  end
end
