class CreateMovies < ActiveRecord::Migration[6.0]
  def change
    create_table :movies do |t|
      t.string :title
      t.string :year
      t.string :genre
      t.time   :duration
      t.text :sinopsis
      t.string :rating
      t.timestamps
    end
  end
end
