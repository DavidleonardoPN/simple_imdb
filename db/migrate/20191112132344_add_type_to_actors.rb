class AddTypeToActors < ActiveRecord::Migration[6.0]
  def change
      add_column :actors, :type, :string
  end
end
