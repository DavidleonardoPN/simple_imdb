class CreateMoviesDirectors < ActiveRecord::Migration[6.0]
  def change
    create_table :movies_directors do |t|
      t.references :movie, index: true
      t.references :actor, index: true,  foreign_key: 'director_id'
      t.timestamps  null: false
    end
  end
end
